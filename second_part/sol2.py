#part 2
#exo 1
import random
def random_gen():
    while (True):
        r=random.randint(10, 20)
        if r != 15 :
            print(r)
        else :
            break
random_gen()  

#exo 2
def add(a , b):
    return int(a) + int(b)
def get_info(func):
    return "The function " + func.__name__ + " was passed to get_info the resulte is : "+ str(func('5',8))
get_info(add)

#exo 3
try:
    # get_info(add)
    
except Exception:
    pass
finally:
    clean_up

#exo 4
class CacheDecorator :
    def __init__(self, a, b):
        self.a = a
        self.b = b
    
    def get_info(self):
        print("The resulte is : "+ str(int(self.a) + int(self.b)))

x = CacheDecorator('5',2)   
x.get_info()

#exo 5
class ForceToList :
  
  def __init__(self, fname, lname):
    self.firstname = fname
    self.lastname = lname

  def printname(self):
    print(self.firstname, self.lastname)

#Use the ForceToList class to create an object, and then execute the printname method:

x = ForceToList ("Menouer", "Ramzi")
x.printname()

class MetaInherList (ForceToList):
    pass
x = MetaInherList("Menouer", "Anouar")
x.printname()

#exo 6

import inspect
class Test :
    process = ""
    a = 20
    b = 30
    
class Metaclass :
    pass
    def check(self):
        attributes = inspect.getmembers(Test, lambda a:not(inspect.isroutine(a)))
        print([a for a in attributes if not(a[0].startswith('__') and a[0].endswith('__'))])
        return [a[0]=='process' for a in attributes if not(a[0].startswith('__') and a[0].endswith('__'))]
        
x= Metaclass()
x.check()

