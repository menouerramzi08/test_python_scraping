#part 3
#exo 1
import requests
pload = {'isadmin': 1}
r = requests.post('https://httpbin.org/anything',data = pload)
print(r.text)
r_dictionary= r.json()
print(r.status_code)
print(r_dictionary['form'])

#exo 2
import json
import csv

Dict = {}
Rows = []
with open('data.json') as f:
    data = json.load(f)
    Bundles =data['Bundles']
    for Bun in Bundles :
        Product = Bun
        Pro = Product['Products']
        Produit = Pro[0]
        name = Produit['Name']
        Price = Produit['Price']
    
        Dict[name] ="%.1f" %float(Price)
    
        Rows.append((name ,'%.1f' %float(Price)))
print(Dict) 
header = ['Product_Name', 'Product_Price']

with open('countries.csv', 'w', encoding='UTF8') as c:
    writer = csv.writer(c)

    # write the header
    writer.writerow(header)
    writer.writerow(Rows)

#exo 3 
from bs4 import BeautifulSoup
import urllib.request
import csv




urlpage = 'https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas'

headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3"}

# query the website and return the html to the variable 'page'

req = urllib.request.Request(url=urlpage, headers=headers)
page = urllib.request.urlopen(req).read()

# parse the html using beautiful soup and store in variable 'soup'

soup = BeautifulSoup(page, 'html.parser')
print(soup)

# find results within table

table = soup.find('div', attrs={'class': 'ng-tns-c199-3 ng-trigger ng-trigger-staggerFadeInOut product-grid'})
print(table)
results = table.find_all('div' , attrs={'class': 'shelfProductTile-information'})
print('Number of results', len(results))

# create and write headers to a list

rows = []
rows.append(["Title", "PrixL", "PrixT"])
print(rows)

# loop over results

data = []
for result in results:
  # find all columns per result
  data.append(result.find_all('div' , attrs={'class':'shelfProductTile-information'}))
  # check that columns have data
  if len(data) == 0:
    continue
data    

# write columns to variables

PrixL=[]
Title=[]
PrixT=[]

data2 =[]

for i in range(len(data)):
  Title.append(data[i][0].find('a' , attrs={'class':'shelfProductTile-descriptionLink'}).getText())
  PrixL.append(data[i][0].find('div' , attrs={'class':'shelfProductTile-cupPrice ng-star-inserted'}).getText())
  prix=data.find_all('div' , attrs={'class':'price price--large ng-star-inserted'}).getText()
  PrixT.append((prix.find('span' , attrs={'class' :'price-dollars'})+','+prix.find('div' , attrs={'class' :'price-centsPer'})))  
  data2.append((Title[i] ,PrixL[i] ,PrixT[i]))


rows.append(data2)

# Create csv and write rows to output file

with open('res.csv','w', newline='') as f_output:
  csv_output = csv.writer(f_output)
  csv_output.writerows(rows)
